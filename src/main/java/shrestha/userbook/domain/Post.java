package shrestha.userbook.domain;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Post {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @NotNull
    private int postID;
    private String message;
    private String emailID;

    public Post(){
    }

    public Post(int postID, String message, String emailID) {
        this.postID = postID;
        this.message = message;
        this.emailID = emailID;
    }

    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }


    @Override
    public String toString() {
        return "Post{" +
                "postID=" + postID +
                ", message='" + message + '\'' +
                ", emailID='" + emailID + '\'' +
                '}';
    }
}


