package shrestha.userbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import shrestha.userbook.domain.Post;
import shrestha.userbook.domain.User;
import shrestha.userbook.service.PostService;
import shrestha.userbook.service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;

    @GetMapping("/admin/users")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    /*Back end service*/
    @PostMapping("/sign-up")
    public void signUp(@ModelAttribute User newUser ){ userService.addUser(newUser);
    }

    @GetMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password, Model model){
        User user = userService.authenticateUser(username, password);
        model.addAttribute(user);
        return "userbook";

    }

    /*Front end service*/
    @GetMapping("/login-home")
    public String loginHome(){return "login";}

    @GetMapping("/signup-home")
    public String signupHome(){return "sign-up";}

    @GetMapping("/post")
    public String post(@ModelAttribute Post post, Model model){
        List<String> posts = postService.postMessage(post);
        model.addAttribute("posts", posts);
        return "post-message";
    }













}
