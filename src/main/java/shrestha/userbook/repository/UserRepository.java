package shrestha.userbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shrestha.userbook.domain.User;

@Repository
public interface UserRepository extends JpaRepository <User, String> {

    @Query(nativeQuery = true, name ="authenticateUser")
    User authenticateUser(@Param(value = "username") String username, @Param(value = "password") String password);
}
