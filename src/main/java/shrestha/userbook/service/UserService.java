package shrestha.userbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shrestha.userbook.domain.User;
import shrestha.userbook.repository.UserRepository;
import shrestha.userbook.utility.PasswordCriteria;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordCriteria passwordCriteria;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void addUser(User newUser){
        String newPassword = newUser.getPassword();
        boolean p1 = passwordCriteria.CheckIfPasswordIsAtleast10Characters(newPassword);
        boolean p2 = passwordCriteria.CheckIfPasswordIsOnlyLettersAndDigit(newPassword);
        boolean p3 = passwordCriteria.IfAtleast2Digits(newPassword);
        boolean p4 = passwordCriteria.IfAtLeastUpperCase(newPassword);
        if(p1 == true && p2 == true && p3 == true && p4 == true){
            userRepository.save(newUser);
        }
    }

    public User authenticateUser(String username, String password) {
        return userRepository.authenticateUser(username, password);
    }

}
