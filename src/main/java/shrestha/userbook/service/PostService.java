package shrestha.userbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shrestha.userbook.domain.Post;
import shrestha.userbook.repository.PostRepository;

import java.util.List;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    public List<String> getMessages(String emailID){
        return postRepository.getMessages(emailID);
    }

    public List<String> postMessage(Post newPost){
        System.out.println(newPost);
        postRepository.save(newPost);
        return getMessages(newPost.getEmailID());

    }
}
