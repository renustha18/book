package shrestha.userbook.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import shrestha.userbook.domain.User;
import shrestha.userbook.repository.UserRepository;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    User user;

    @Before
    public void initializeVariable(){
        user = new User ("renustha18@gmail.com", "renu", "shrestha", "USA", 1234567890);
    }

    @Test
    public void getAllUsersTest() {
        List<User> expected = Arrays.asList(user);
        Mockito.when(userRepository.findAll()).thenReturn(expected);
        List<User> actual = userService.getAllUsers();
        Assert.assertEquals(expected, actual);
    }




}
